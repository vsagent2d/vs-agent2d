// -*-c++-*-

/*
 *Copyright:

 Copyright (C) Hidehisa AKIYAMA

 This code is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3, or (at your option)
 any later version.

 This code is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this code; see the file COPYING.  If not, write to
 the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

 *EndCopyright:
 */

/////////////////////////////////////////////////////////////////////

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "bhv_basic_move.h"

#include "strategy.h"

#include "bhv_basic_tackle.h"

#include <rcsc/action/basic_actions.h>
#include <rcsc/action/body_go_to_point.h>
#include <rcsc/action/body_intercept.h>
#include <rcsc/action/neck_turn_to_ball_or_scan.h>
#include <rcsc/action/neck_turn_to_low_conf_teammate.h>

#include <rcsc/player/player_agent.h>
#include <rcsc/player/debug_client.h>
#include <rcsc/player/intercept_table.h>

#include <rcsc/common/logger.h>
#include <rcsc/common/server_param.h>

#include "neck_offensive_intercept_neck.h"

#include <cstdio> //これを追加するとCで使えるprintfなどがC++でも使用できる。

using namespace rcsc;

/*-------------------------------------------------------------------*/
/*!

 */
bool
Bhv_BasicMove::execute( PlayerAgent * agent )
{
    dlog.addText( Logger::TEAM,
                  __FILE__": Bhv_BasicMove" );

    //-----------------------------------------------
    // tackle
    if ( Bhv_BasicTackle( 0.8, 80.0 ).execute( agent ) )
    {
        return true;
    }

    const WorldModel & wm = agent->world();

    int cycle_num = wm.time().cycle();//これで現在のサイクル数を取得する。

    if(2950 <= cycle_num && cycle_num <= 3050){//現在のサイクル数がこの範囲のとき実行される。

    	int our_score = (wm.ourSide() == LEFT ? wm.gameMode().scoreLeft() : wm.gameMode().scoreRight());//自分のチームの得点を取得する。
    	int opp_score = (wm.ourSide() == RIGHT ? wm.gameMode().scoreLeft() : wm.gameMode().scoreRight());//相手のチームの得点を取得する。

      //以下で試合が終了したときの処理を実行する。
    	if(our_score > opp_score){
    		printf("あなたのチームの勝ちです！\n");
    	}

    	else if(opp_score > our_score){
    		printf("あなたのチームの負けです！\n");
    	}

    	else{
    		printf("引き分けでした！");
    	}

    }

    printf("(現在のボールのx座標,現在のボールのy座標) = (%.1lf,%.1lf)\n",wm.ball().pos().x,wm.ball().pos().y);//端末上に現在のボールのx座標y座標を表示できる。(prinfは上記の cstdio をインクルードしなければ使えない)

    std::cout << "現在のサイクル数 :" << cycle_num << std::endl;//これがC++におけるCのprintfの役割を持ち、これで現在のサイクル数を表示できる。(wm.time().cyclel()で現在のサイクル数を取得している。これは上記でcycle_numとして宣言している。)

    printf("ボールを持っている自分のユニフォーム番号 :%d\n",wm.self().unum());//自分のユニフォーム番号を表示する。(もちろん上記のstd::count の書き方でも良い。)


    /*--------------------------------------------------------*/
    // chase ball

    //ボールに到達するまでのサイクル数 つまり　距離みたいなもの
    const int self_min = wm.interceptTable()->selfReachCycle();//自分がボールまでたどり着く距離
    const int mate_min = wm.interceptTable()->teammateReachCycle();//見方がボールまでたどり着く距離
    const int opp_min = wm.interceptTable()->opponentReachCycle();//敵がボールまでたどり着く距離

    if(self_min <= mate_min)//bhv_basic_move.cpp は自分チームのプレイヤー全員が毎回読み込むためこの条件で、自分チームの選手のうち一番近い選手が読み込まれたときに、自分から一番近い味方の背番号を表示させる。

  	     std::cout <<"自分から一番近い味方" << wm.teammatesFromSelf().front()->unum()<< std::endl;//これで自分から一番近い味方選手の背番号を表示できる。

    if ( ! wm.existKickableTeammate()
         && ( self_min <= 3
              || ( self_min <= mate_min
                   && self_min < opp_min + 3 )
              )
         )
    {
        dlog.addText( Logger::TEAM,
                      __FILE__": intercept" );
        Body_Intercept().execute( agent );
        agent->setNeckAction( new Neck_OffensiveInterceptNeck() );

        return true;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////追加部分

    else if  (( !wm.existKickableTeammate()//!wm.existKickableTeammate()の条件はボールを蹴ることができないときという意味

         &&  (self_min <= 10//自分がボールに10サイクル以内に到達することができるときという意味

           && (wm.self().unum() == 9 || wm.self().unum() == 10 ||
        		 wm.self().unum() == 11)//自分の背番号が9,10,11番のときという意味

               && wm.ball().pos().x >= 0)) ||//現在のボールのx座標が0以上のときという意味
                (( !wm.existKickableTeammate())//味方がボールを蹴ることができない時という意味

                 && (self_min <= mate_min//自分の方がボールまで一番近いときという意味(bhv_basic_moveはすべての味方選手で読み込むため)
            		   && self_min < opp_min + 3))//敵がボールに到達するまでの距離 + 3よりも自分の方がボールに近い時という意味

			   )


    {
        dlog.addText( Logger::TEAM,
                      __FILE__": intercept" );
        Body_Intercept().execute( agent );
        agent->setNeckAction( new Neck_OffensiveInterceptNeck() );

        return true;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////

    if(wm.getOpponentNearestToSelf(10, false)->unum()){//一番近い敵がの情報が定義できない場合があるみたいなので必要な条件(敵の情報を得ることができない時は返り値として0が変えるので実行されない)

	     std::cout << "opponentNearesrToSelf :" << wm.getOpponentNearestToSelf(10, false)->unum() << std::endl;//第一引数は先輩に聞いたら意味がわかったのですが少し難しいので知りたい人は僕に聞いてください。第二引数はゴールキーパーを味方とするかの設定です。これで自分から一番近い敵までの距離を表示できる。(bhv_basic_moveは全味方プレイヤーに対して読み込まれるので)

    }

    const Vector2D target_point = Strategy::i().getPosition( wm.self().unum() );

    const Vector2D target_point2 = wm.getOpponentNearestToSelf(10, false)->pos();//一番近い敵をマークするために、敵の座標を取得する。

    const double dash_power = Strategy::get_normal_dash_power( wm );

    const Vector2D seven = Strategy::i().getPosition(7);//これで味方の座標を取得できる。

    if(seven){//これも上記と同じで、座標が得られない時は0が返り実行されない。

      std::cout << "背番号7番の座標 :" << seven << std::endl;//これで背番号7番の座標を表示できる。

    }

    double dist_thr = wm.ball().distFromSelf() * 0.1;
    if ( dist_thr < 1.0 ) dist_thr = 1.0;

    dlog.addText( Logger::TEAM,
                  __FILE__": Bhv_BasicMove target=(%.1f %.1f) dist_thr=%.2f",
                  target_point.x, target_point.y,
                  dist_thr );

    agent->debugClient().addMessage( "BasicMove%.0f", dash_power );
    agent->debugClient().setTarget( target_point );
    agent->debugClient().addCircle( target_point, dist_thr );


    if(self_min >= opp_min){//敵の方がボールに近い時つまり自分たちのチームが守備側の状態のとき

    	if(! Body_GoToPoint(target_point2,dist_thr,dash_power).execute(agent)){//もし一番近い敵を味方がマークできる時はtarget_point2 で取得した座標に味方選手がマークしに向かうが、マークできない時は0が返り値として渡されるのでこの処理は実行されない。

    		Body_TurnToBall().execute(agent);//ボールを味方選手がマークしたあとに、ボールを見失ってしまわないようにボールの方を向かせる。

    	}

    }

    if ( ! Body_GoToPoint( target_point, dist_thr, dash_power
                           ).execute( agent ) )
    {
        Body_TurnToBall().execute( agent );
    }

    if ( wm.existKickableOpponent()
         && wm.ball().distFromSelf() < 18.0 )
    {
        agent->setNeckAction( new Neck_TurnToBall() );
    }
    else
    {
        agent->setNeckAction( new Neck_TurnToBallOrScan() );
    }

    return true;
}
